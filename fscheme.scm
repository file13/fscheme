;#lang scheme
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  Fscheme: Scheme source storing system...kinda like Forth's 
;;;  words dictionary and the word WORDS.
;;;
;;;  Michael Rossi
;;;  dionysius.rossi@gmail.com
;;;  Last update: Jan 12, 2007
;;;
;;;  This is public domain.  If you want to use it, have fun.
;;;
;;;  Purpose: Create a *sources* list to store all source documentation
;;;  like in Forth and Common Lisp's documentation
;;;  The goal is to write:
;;;  (def double1 (lambda (x) (* 2 x)))
;;;  (def (double2 x) (* 2 x))
;;;  and have Scheme store both the sources of the file
;;;  in *sources* for later lookup.
;;;
;;;  Notes: This only uses R5RS procedures. Efficiency is not a concern.
;;;  Tested: Mzscheme 360, Bigloo 2.8c, Scheme48 1.4
;;;
;;;  Todo: 
;;;   1. Think of way to do *sources* without global so mzscheme 
;;;      can use this in a module.
;;;   2. Add a way to drop a source or re-implement as a set.
;;;   3. Add a way serach for multiple sources (unless set).
;;;
;;;  Usage: Once loaded, if you define something with def
;;;  instead of define, the source will be put into the global
;;;  list *sources*.  Example:
;;;
;;;  (def (puts str) (display str) (newline))
;;;
;;;  To view all the sources you can simple run 
;;;  (srcs)
;;;
;;;  You can also get a list of the *sources* by going
;;;  (srcs-list)
;;;
;;;  If you want to see the source of a procedure go:
;;;  (src 'puts)
;;;
;;;  Like most Forth's, it currently does not allow you to drop
;;;  sources from the list and if you redefine a procedure, the
;;;  last source will be the one stored and retrieved.
;;;
;;;  That's all for now.
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;                                                          
;                                                          
;                                                          
;                                                          
;   ;;;;;;   ;;; ;          ;;                             
;    ;   ;  ;   ;;           ;                             
;    ; ;    ;        ;;; ;   ; ;;    ;;;;  ;; ;  ;   ;;;;  
;    ;;;     ;;;;   ;   ;;   ;;  ;  ;    ;  ;; ;; ; ;    ; 
;    ; ;         ;  ;        ;   ;  ;;;;;;  ;  ;  ; ;;;;;; 
;    ;           ;  ;        ;   ;  ;       ;  ;  ; ;      
;    ;      ;;   ;  ;    ;   ;   ;  ;       ;  ;  ; ;      
;   ;;;     ; ;;;    ;;;;   ;;; ;;;  ;;;;; ;;; ;; ;  ;;;;; 
;                                                          
;                                                          
;                                                          
;                                                  

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  If you decide not to use this later in "production" code
;;  for efficency reasons, just add this macro to override
;;  the previous version of def in this library.  You still
;;  get the benefit of typing three less characters without
;;  the overhead.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;(define-syntax def
;  (syntax-rules ()
;    ((_ name ...)
;     (eval (list 'define 'name ...) 
;           (interaction-environment)))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-syntax def
  (syntax-rules ()
    ((_ name ...)
     (source-define (list 'define 'name ...)))))
;;  Macro to build a proper "define" list that can be
;;  added to the *sources* and then eval'ed
  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  These are for our sanity when doing look ups in
;;  *sources* list
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define-syntax first-after-define
  (syntax-rules ()
    ((_ x)
     (cadar x))))

(define-syntax function-name-without-lambda
  (syntax-rules ()
    ((_ x)
     (caadar x))))

  ;;  Our global list to hold all the sources
(define add-source!
  (lambda (src)
    (set! *sources* (cons src *sources*))))
;;  Function to add a source to the *sources*

(define source-define
  (lambda (proc)
    (eval proc (interaction-environment))
    (add-source! proc)))
;;  Works with the function handed to it by def below.
;;  First it adds the source then eval's it as normal.


;;  Now we re-define the previous procedures so they
;;  will also show up in the *sources*
(def *sources* '())
    
(def add-source!
     (lambda (src)
       (set! *sources* (cons src *sources*))))

(def source-define
     (lambda (proc)
       (eval proc (interaction-environment))
       (add-source! proc)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  This finds a source in any list of defines    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def find-source
     (lambda (proc-sym source)
       (cond ((null? source) '())
             ((pair? (first-after-define source))
                                        ; if true we know it's an abbreviated definition
              (cond ((eq? proc-sym (function-name-without-lambda source))
                     (car source))
                    (else (find-source proc-sym (cdr source)))))
                                        ; we know it's a normal definition with lambda
             (else (cond ((eq? proc-sym (first-after-define source))
                          (car source))
                         (else (find-source proc-sym (cdr source))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  Shortcut to grab the source
;;  use this like so to grab a procedure's definition:
;;  (src 'add-source!)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def src
     (lambda (fun-sym)
       (find-source fun-sym *sources*)))
    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  Return a list of all the *sources* names.
;;  Comes in handy when mapping all the sources.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def srcs-list
     (lambda ()
       (letrec 
           ((rec (lambda (s)
                   (let ((src-list '()))
                     (cond ((null? s) '())
                           (else 
                            (cond ((pair? (first-after-define s))
                                   (cons (function-name-without-lambda s)
                                         (rec (cdr s))))
                                  (else
                                   (cons (first-after-define s)
                                         (rec (cdr s)))))))))))
         (rec *sources*))))
    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  Display all the *sources* names
;;  map-ing (srcs-list) adds a bunch of ugly values
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def srcs
     (lambda ()
       (letrec 
           ((rec 
             (lambda (s)
               (cond ((null? s) (newline))
                     (else 
                      (cond ((pair? (first-after-define s))
                             (display (function-name-without-lambda s))
                             (display " ")
                             (rec (cdr s)))
                            (else
                             (display (first-after-define s))
                             (display " ")
                             (rec (cdr s)))))))))
         (rec *sources*))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;  This is the end of the actual fscheme procedures
;;;
;;;  The rest is just tests and regular libs.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;
;
;
;
;   ;;;;;            ;;;;;          ;;;        ;   ;;
;    ;   ;           ;               ;              ;
;    ;   ;   ;;;;;   ;               ;       ;;;    ; ;;;    ;;;;;
;    ;   ;  ;    ;   ;;;;            ;         ;    ;;   ;  ;    ;
;    ;;;;    ;;;;        ;           ;         ;    ;    ;   ;;;;
;    ;  ;        ;       ;           ;   ;     ;    ;    ;       ;
;    ;   ;  ;    ;  ;    ;           ;   ;     ;    ;;   ;  ;    ;
;   ;;;   ; ;;;;;    ;;;;           ;;;;;;   ;;;;; ;; ;;;   ;;;;;
;
;
;
;
  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;  From the Little Schemer
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def atom?
     (lambda (x)
       (and (not (pair? x)) (not (null? x)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Printing functions
;;;  using various lambda style to test
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def put
     (lambda (str)
       ;; shortcut to display...really lazy
       (display str)))

(def puts
     ;; shorcut to print
     (lambda (str)
       (display str)
       (newline)))

(def (putss str)
     ;; handy for printing lists"
     (display str)
     (display " "))

(def (nl)
     ;; shortcut for newline
     (newline))

(def string-divider
     (lambda (num . cha)
       ;; return a string banner divider
       (cond ((null? cha)
              (make-string num #\-))
             (else
              (make-string num (car cha))))))

(def print-divider
     (lambda (num . cha)
       ;; prints a string banner divider
       (cond ((null? cha)
              (puts (make-string num #\-))
              (newline))
             (else
              (puts (make-string num (car cha)))
              (newline)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Read functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def flush
     (lambda ()
       ;; flush the buffer
       (cond ((char-ready?)
              (read-char)
              (flush))
             (else #f))))

(def readl
     (lambda ()
       ;; Reads a string until it get's a #\newline
       (letrec
           ((rec
             (lambda (str)
               (let ((c (read-char (current-input-port))))
                 (cond ((or (char=? c #\newline) (eof-object? c))
                        str)
                       (else
                        (rec (string-append str (make-string 1 c)))))))))
         (flush)
         (rec (string)))))

(def prompt
     (lambda (prompt-string)
       ;; Displays a prompt and returns a string from current-input-port
       (display prompt-string)
       (readl)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; List functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def putl
     (lambda (lst)
       ;; Print each item in a list on a newline
       (for-each puts lst)))

(def putls
     (lambda (lst)
       ;; Print each item in a list with a space
       (for-each putss lst)))

(def list->file
     (lambda (lst filename)
       ;; Write a file from a list
       (let ((out-file (open-output-file filename)))
         (for-each (lambda (i)
                     (display i out-file)
                     ;;(display #\return out-file)
                     (display #\newline out-file)) lst)
         (close-output-port out-file))))

